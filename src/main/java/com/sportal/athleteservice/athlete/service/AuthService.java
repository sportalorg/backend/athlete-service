package com.sportal.athleteservice.athlete.service;

import com.sportal.athleteservice.athlete.integration.Auth0Integration;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final Auth0Integration auth0Integration;

    public String getJwks() {
        return auth0Integration.getJwks();
    }

}
