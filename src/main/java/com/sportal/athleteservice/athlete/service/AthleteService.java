package com.sportal.athleteservice.athlete.service;

import com.sportal.athleteservice.athlete.dto.AthleteDisplayNames;
import com.sportal.athleteservice.athlete.dto.AthleteRequest;
import com.sportal.athleteservice.athlete.dto.AthleteResponse;
import com.sportal.athleteservice.athlete.entity.Athlete;
import com.sportal.athleteservice.athlete.repository.AthleteRepository;
import com.sportal.athleteservice.utils.AuthUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AthleteService {

    private final AuthUtils authUtils;
    private final AthleteRepository athleteRepository;
    public AthleteResponse createAthlete(String accessToken, AthleteRequest athleteRequest) throws JSONException {
        String sub = authUtils.getField(accessToken, "sub");

        Optional<Athlete> existingAthlete = athleteRepository.findById(sub);
        if(existingAthlete.isEmpty()){
            Athlete athlete = athleteRepository.save(
                    Athlete.builder()
                            .id(sub)
                            .email(athleteRequest.email())
                            .displayName(athleteRequest.displayName())
                            .build());
            return AthleteResponse.builder()
                    .id(athlete.getId())
                    .email(athlete.getEmail())
                    .displayName(athlete.getDisplayName())
                    .build();
        }
        return AthleteResponse.builder()
                .id(existingAthlete.get().getId())
                .email(existingAthlete.get().getEmail())
                .displayName(existingAthlete.get().getDisplayName())
                .build();

    }
    public AthleteResponse getAllAthleteDetail(String accessToken) throws JSONException {
        String sub = authUtils.getField(accessToken, "sub");
        Optional<Athlete> athlete = athleteRepository.findById(sub);
        if(athlete.isEmpty()){
            return null;
        }
        return AthleteResponse.builder()
                .id(athlete.get().getId())
                .email(athlete.get().getEmail())
                .displayName(athlete.get().getDisplayName())
                .build();

    }

    public List<AthleteDisplayNames> getAthleteDisplayNames(List<String> athleteIds){

        return athleteRepository.findAllById(athleteIds)
                .stream()
                .map(a -> AthleteDisplayNames.builder()
                        .id(a.getId())
                        .displayName(a.getDisplayName()).build())
                .toList();
    }
}
