package com.sportal.athleteservice.athlete.dto;

import lombok.Builder;

@Builder
public record AthleteResponse(
        String id,
        String email,
        String displayName
) { }

