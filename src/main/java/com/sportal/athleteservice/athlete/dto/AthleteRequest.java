package com.sportal.athleteservice.athlete.dto;

import lombok.Builder;

@Builder
public record AthleteRequest(
        String email,
        String displayName
) { }
