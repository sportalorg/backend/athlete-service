package com.sportal.athleteservice.athlete.dto;

import lombok.Builder;

@Builder
public record AthleteDisplayNames(
        String id,
        String displayName
) {
}
