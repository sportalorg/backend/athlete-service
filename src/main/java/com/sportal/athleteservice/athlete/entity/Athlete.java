package com.sportal.athleteservice.athlete.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "athletes")
public class Athlete {

    @Id
    private String id;
    private String email;
    private String displayName;


}
