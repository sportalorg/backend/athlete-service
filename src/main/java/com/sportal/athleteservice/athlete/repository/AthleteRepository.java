package com.sportal.athleteservice.athlete.repository;

import com.sportal.athleteservice.athlete.entity.Athlete;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AthleteRepository extends MongoRepository<Athlete, String> {

    List<Athlete> findByEmail(String email);
}
