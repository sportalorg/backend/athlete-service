package com.sportal.athleteservice.athlete.controller;

import com.sportal.athleteservice.athlete.dto.AthleteRequest;
import com.sportal.athleteservice.athlete.dto.AthleteResponse;
import com.sportal.athleteservice.athlete.service.AthleteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AthleteController {

    private final AthleteService athleteService;

    @PostMapping("/athlete")
    public ResponseEntity<?> addAthlete(@RequestHeader(name = "Authorization") String accessToken,
                                        @RequestBody AthleteRequest athleteRequest) throws JSONException {

        AthleteResponse athlete = athleteService.createAthlete(accessToken, athleteRequest);

        return new ResponseEntity<>(athlete, HttpStatus.CREATED);
    }

    @GetMapping("/athlete/mydetails")
    public ResponseEntity<?> getMyDetails(@RequestHeader(name = "Authorization") String accessToken) throws JSONException {
        AthleteResponse athleteResponse = athleteService.getAllAthleteDetail(accessToken);
        if(athleteResponse == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(athleteResponse, HttpStatus.OK);
    }

    @GetMapping("/athlete")
    public ResponseEntity<?> getAthleteDisplayNames(@RequestParam List<String> athleteIds){
        return new ResponseEntity<>(athleteService.getAthleteDisplayNames(athleteIds), HttpStatus.OK);
    }


}
